// src/components/Login.js
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import './Login.css';

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const history = useHistory();

  const handleLogin = () => {
    // Thực hiện xác thực ở đây. Đây chỉ là ví dụ đơn giản.
    if (username === 'user' && password === 'password') {
      // Đăng nhập thành công, chuyển hướng đến trang chính
      history.push('/home');
    } else {
      // Đăng nhập không thành công, xử lý thông báo hoặc thực hiện các hành động khác
      alert('Đăng nhập không thành công. Vui lòng kiểm tra thông tin đăng nhập.');
    }
  };

  return (
    <div className="login-container">
      <h2>Login</h2>
      <label>
        Username:
        <input type="text" value={username} onChange={(e) => setUsername(e.target.value)} />
      </label>
      <br />
      <label>
        Password:
        <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
      </label>
      <br />
      <button onClick={handleLogin}>Login</button>
    </div>
  );
};

export default Login;
